# Sprint Stats

Generates stats for previous sprints.

## Pre-requisites
You will need to set up two local environment variables:
* JIRA_EMAIL = USERNAME@msts.com
* JIRA_KEY = API access token from https://id.atlassian.com/manage/api-tokens

## Configuration
First modify command field in docker-compose.yml.

Examples for Kanban teams:
* `python3 getKanbanStats.py BCA -noweekend`
* `python3 getKanbanStats.py BCA,PI -noweekend`

For Sprint-based teams:
* Sprint Name (in quotes)
* Sprint Start Date (YYYY-MM-DD format)

Example:
* `python3 getSprintStats.py "Thunder Chicken Flipper" 2019-01-29 -noweekends`

Note:
If the team works during the weekend, don't pass -noweekend.

## Usage:
For Kanban teams:
*  `docker compose up && open outputKanban.html`

For Sprint-based teams:
*  `docker compose up && open output.html`